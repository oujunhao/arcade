const mongoose = require("mongoose");

const gameSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  publisher: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now()
  },
  newUntil: {
    type: Date,
    default: Date.now() + 604800 // one week in seconds
  },
  description: {
    type: String,
    required: true
  },
  previewImagePath: {
    // path to preview image
    type: String,
    required: true
  },
  exePath: {
    // path to exe
    type: String,
    required: true
  },
  featured: {
    // if the game is featured or not
    type: Boolean,
    default: false
  },
  endFeature: Date,
  featuredBy: String
});

var Game = mongoose.model("Game", gameSchema);

module.exports = Game;
