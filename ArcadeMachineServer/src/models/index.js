//DEPENDENCIES
var mongoose = require("mongoose");
var Student = require("./Student");
var Game = require("./Game");
var Session = require("./Session");

//CONFIG
// const DB_IP = "138.197.136.94";
// const DB_PORT = "27017";
//mongodb://${DB_IP}:${DB_PORT}/arcade
const DB_PORT = "27017";
const DB_IP = "localhost";


//MONGOOSE CONNECTION
mongoose.connect(
  `mongodb://${DB_IP}:${DB_PORT}/arcade`,
  {
    keepAlive: true
  },
  //eslint-disable-next-line
  () => console.log(`Connected to database at ${DB_IP}:${DB_PORT}`)
);

//MONGOOSE PROMISE CONFIG
mongoose.set("debug", true);
mongoose.Promise = Promise;

module.exports.game = Game;
module.exports.session = Session;
module.exports.student = Student;
