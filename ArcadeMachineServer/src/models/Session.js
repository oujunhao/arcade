const mongoose = require("mongoose");

const sessionSchema = new mongoose.Schema({
  cardId: {
    type: String,
    required: true
  },
  gameId: {
    type: String,
    required: true
  },
  startTime: {
    type: Date,
    required: true
  },
  endTime: {
    type: Date
  },
  score: {
    type: Number
  }
});

var Session = mongoose.model("Session", sessionSchema);

module.exports = Session;
