const express = require("express");
const {
  startSession,
  endSession,
  getSessionsByCardId,
} = require("../handlers/session");
const router = express.Router();

router.post("/startSession", startSession);

router.post("/endSession", endSession);

router.get("/:cardId", getSessionsByCardId);

module.exports = router;
