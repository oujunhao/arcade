var express = require("express");
var {
  getGameIndex,
  enterGame,
  getGame,
  createOrUpdateGame,
  deleteGame
} = require("../handlers/game");
var { getScoreRange } = require("../handlers/session");
var router = express.Router();


router.route("/")
  .get(getGameIndex) //INDEX - show a list of all the games in the database
  .put(createOrUpdateGame); //PUT - create or update a game in the database


router.route("/:gameId")
  .get(getGame)         // SHOW - get info about a game in the database
  .delete(deleteGame);  // DELETE - remove a game form the database

// Enter game, return json web token
router.get("/enterGame/:gameId/:cardId", enterGame);

//READ - get a range of scores from sessions
router.get("/:gameToken/scores/:skip/:limit", getScoreRange);

module.exports = router;
