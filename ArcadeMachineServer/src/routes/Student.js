const express = require("express");
const { getStudent, createStudent } = require("../handlers/student");

const router = express.Router();

//INDEX
router.get("/:cardId", getStudent);
router.post("/createStudent", createStudent);

module.exports = router;
