const db = require("../models");
const status200 = require("./status200");
const sendError = require("./sendError");

exports.createStudent = async function(req, res, next) {
  const { name, cardId } = req.body;
  try {
    return status200(res, {
      requestType: "create-student",
      student: await db.student.create({ name, cardId })
    });
  } catch (err) {
    sendError(err, next);
  }
};

exports.getStudent = async function(req, res, next) {
  try {
    return status200(res, {
      requestType: "get-student",
      student: await findStudentFromCardId(req.params.cardId)
    });
  } catch (err) {
    sendError(err, next);
  }
};

async function findStudentFromCardId(cardId) {
  let student = await db.student.findOne({ cardId });
  return student;
}
