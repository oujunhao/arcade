module.exports = function error404(req, res, next){
  let err = new Error("not found");
  err.status = 404;
  next(err);
};
