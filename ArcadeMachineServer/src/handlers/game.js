var jwt = require("jsonwebtoken");
var mongoose = require("mongoose");

var config = require("../../config");

var db = require("../models");
const nullError = require("./nullError");
const status200 = require("./status200");
const sendError = require("./sendError");

//INDEX
exports.getGameIndex = async function(req, res, next) {
  try {
    return status200(res, {
      requestType: "get-game-index",
      foundGames: await getGames()
    });
  } catch (err) {
    sendError(err, next);
  }
};

async function getGames() {
  let games = await db.game.find({});
  let newGames = await Promise.all(games.map(game => getGame(game._id)));
  return newGames;
}

//SHOW
exports.getGame = async function(req, res, next) {
  try {
    return status200(res, {
      requestType: "get-game",
      foundGame: await getGame(req.params.gameId)
    });
  } catch (err) {
    sendError(err, next);
  }
};

async function getGame(gameId) {
  var foundGame = await db.game.findById(gameId);
  nullError(foundGame, "game", gameId);
  foundGame._doc.isNew = isNewGame(foundGame);
  foundGame._doc.numberPlays = await getNumberGamePlays(gameId);
  return foundGame;
}

function isNewGame(foundGame) {
  return Date.now() < foundGame.newUntil;
}

async function getNumberGamePlays(gameId) {
  const plays = await db.session.find({ gameId });
  return plays.length;
}
//PUT
exports.createOrUpdateGame = async function(req, res, next) {
  try {
    return status200(res, {
      requestType: "create-game",
      newOrUpdatedGame: await createNewOrUpdateGame(req)
    });
  } catch (err) {
    sendError(err, next);
  }
};

async function createNewOrUpdateGame(req) {
  const {
    _id,
    name,
    publisher,
    newUntil,
    description,
    featured,
    previewImagePath,
    exePath
  } = req.body;
  const newOrUpdatedGame = await db.game.update(
    { _id: _id ? _id : mongoose.Types.ObjectId() },
    {
      name,
      publisher,
      newUntil,
      description,
      featured,
      previewImagePath,
      exePath
    },
    { upsert: true, setDefaultsOnInsert: true }
  );
  return newOrUpdatedGame;
}

// Enter Game
exports.enterGame = function(req, res, next) {
  try {
    return status200(res, {
      requestType: "enter-game",
      gameToken: createGameToken(req)
    });
  } catch (err) {
    sendError(err, next);
  }
};

function createGameToken(req) {
  const { gameId, cardId } = req.params;
  return jwt.sign({ gameId, cardId }, config.secret);
}

// DESTROY GAME
exports.deleteGame = async function(req, res, next) {
  try {
    return status200(res, {
      requestType: "delete-game",
      deletedGame: await deleteAndReturnGame(req)
    });
  } catch (err) {
    sendError(err, next);
  }
};

async function deleteAndReturnGame(req) {
  const { gameId } = req.params;
  const deletedGame = await db.game.findByIdAndRemove(gameId);
  nullError(deletedGame, "game", gameId);
  return deletedGame;
}
