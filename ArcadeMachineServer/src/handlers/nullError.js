module.exports = function(val, title, id) {
  if (val === null) {
    throw { message: `no ${title} could be found with the given ID: ${id}` };
  }
};
