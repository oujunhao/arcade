module.exports = function sendError(err, next){
  next({ status: 400, message: err.message });
};
