const db = require("../models");
const status200 = require("./status200");


// SALT GAMES, PLAYERS
const games = [
  {
    name: "Game1",
    publisher: "A dude",
    newUntil:34938459,
    description:"this game good",
    previewImagePath:"path/to/image",
    exePath:"path/to/exe",
  },
];

const students = [
  { cardId: 20500001332241, name: "Cameron Teasdale"},
  { cardId: 186674925, name: "Aaron Olsen" },
  { cardId: 56010002010916, name: "John Smith" }
];

module.exports = async (req, res) => {
  await saltStudents();
  await saltGames();
  await saltSessions();
  return status200(res, { requestType: "salt-database" });
};

async function saltStudents() {
  await db.student.remove({});
  await Promise.all([
    createStudents(0),
    createStudents(1),
    createStudents(2)
  ]);
}

async function createStudents(index) {
  await db.student.create(students[index]);
  //eslint-disable-next-line
  console.log("created student " + index);
}


async function saltGames() {
  await db.game.remove({});
  await createGame(0);
}

async function createGame(index) {
  await db.game.create(games[0]);
  //eslint-disable-next-line
  console.log("created game " + index);
}

async function saltSessions() {
  await db.session.remove({});

  for (var i = 0; i < 20; i++) {
    await createSession(i);
  }
  //eslint-disable-next-line
  console.log("salted sessions");
}

async function createSession(index) {
  const game = await db.game.findOne({});
  const gameId = game._id;
  await db.session
    .create({
      cardId: students[rng(students.length)].cardId,
      gameId,
      startTime: 70000,
      endTime: 80000,
      score: Math.floor(Math.random() * 1000)
    });
  //eslint-disable-next-line
  console.log("created session " + index);
}

function rng(to){
  return Math.floor(Math.random() * to);
}
