const jwt = require("jsonwebtoken");

const config = require("../../config");

const db = require("../models");
const status200 = require("./status200");
const sendError = require("./sendError");

/////// /////// /////// /////// /////// READ, get a range of scores from best - worst /////// /////// /////// /////// ///////

exports.getScoreRange = function(req, res, next) {
  try {
    getScoreRange(res, req);
  } catch (err) {
    sendError(err, next);
  }
};

async function getScoreRange(res, req) {
  const { gameToken, skip, limit } = req.params;
  const { gameId } = await decodeIdsFromJWT(gameToken);
  const foundSessions = await getSessionsByGameScoreRanking(
    skip,
    limit,
    gameId
  );
  const foundStudents = await getStudentsFromSessionList(foundSessions);
  const highscores = mapHighscoreListing(foundSessions, foundStudents, skip);

  return status200(res, {
    requestType: `get-score-skip(${skip})-limit(${limit})`,
    highscores: highscores
  });
}

async function decodeIdsFromJWT(gameToken) {
  let gameId;
  let cardId;
  try {
    await jwt.verify(gameToken, config.secret, async (err, decoded) => {
      if (err) throw err;
      gameId = decoded.gameId;
      cardId = decoded.cardId;
    });
  } catch (err) {
    throw err;
  }
  return { gameId, cardId };
}

async function getSessionsByGameScoreRanking(skip, limit, gameId) {
  const scoreRankingSort = { score: -1 };
  return await db.session
    .find({ gameId, cardId: { $ne: "theIgnoredGuestCardId" } })
    .sort(scoreRankingSort)
    .skip(Number(skip))
    .limit(Number(limit));
}

async function getStudentsFromSessionList(foundSessions) {
  return await Promise.all(
    foundSessions.map(session => db.student.findOne({ cardId: session.cardId }))
  );
}

function mapHighscoreListing(foundSessions, foundStudents, skip) {
  return foundSessions.map((session, index) => {
    return {
      Rank: Number(index) + Number(skip) + 1,
      Score: session.score,
      Name: foundStudents[index].name
    };
  });
}

/////// /////// /////// /////// /////// START, start a session and get a json web token /////// /////// /////// /////// ///////

exports.startSession = async function(req, res, next) {
  try {
    startSession(req, res);
  } catch (err) {
    sendError(err, next);
  }
};

async function startSession(req, res) {
  const { gameToken } = req.body;
  const newSession = await createNewSession(gameToken);
  const sessionToken = await createSessionToken(newSession);

  return status200(res, {
    requestType: "start-session",
    sessionToken
  });
}

async function createNewSession(gameToken) {
  const ids = await decodeIdsFromJWT(gameToken);
  return await db.session.create({
    cardId: ids.cardId,
    gameId: ids.gameId,
    startTime: Date.now()
  });
}

async function createSessionToken(session) {
  const { _id } = session;
  return await jwt.sign(
    {
      sessionId: _id
    },
    config.secret
  );
}

/////// /////// /////// /////// /////// END, end a session /////// /////// /////// /////// ///////

exports.endSession = async function(req, res, next) {
  try {
    endSession(req, res);
  } catch (err) {
    sendError(err, next);
  }
};

async function endSession(req, res) {
  const { gameToken, sessionToken, score } = req.body;
  const isHighScoreInfo = await isHighscore(gameToken, score);
  await updateSessionEnd(sessionToken, score);

  return status200(res, {
    requestType: "end-session",
    ...isHighScoreInfo,
    message: "success"
  });
}

async function updateSessionEnd(sessionToken, score) {
  const sessionId = await decodeSessionIdFromJWT(sessionToken);
  return await db.session.findByIdAndUpdate(sessionId, {
    $set: {
      endTime: Date.now(),
      score: Number(score)
    }
  });
}

async function decodeSessionIdFromJWT(sessionToken) {
  let sessionId;
  try {
    await jwt.verify(sessionToken, config.secret, async (err, decoded) => {
      if (err) throw err;
      sessionId = decoded.sessionId;
    });
  } catch (err) {
    throw err;
  }

  return sessionId;
}

async function isHighscore(gameToken, score) {
  const { gameId, cardId } = await decodeIdsFromJWT(gameToken);
  return {
    isPersonalHighscore: await isPersonalHighscore(gameId, cardId, score),
    isGlobalHighscore: await isGlobalHighscore(gameId, score)
  };
}

async function isPersonalHighscore(gameId, cardId, score) {
  return score > (await getPersonalHighscore(gameId, cardId));
}

async function getPersonalHighscore(gameId, cardId) {
  const highscoreSession = await db.session
    .findOne({
      cardId: cardId,
      gameId: gameId
    })
    .sort({ score: -1 })
    .limit(1);
  return highscoreSession.score || 0;
}

async function isGlobalHighscore(gameId, score) {
  return score > (await getGlobalHighscore(gameId));
}

async function getGlobalHighscore(gameId) {
  const highscoreSession = await db.session
    .findOne({ gameId, cardId: { $ne: "theIgnoredGuestCardId" } })
    .sort({ score: -1 })
    .limit(1);
  return highscoreSession != null ? highscoreSession.score : 0;
}

/////// /////// /////// /////// /////// INDEX - get all sessions from a given player with cardId /////// /////// /////// /////// ///////

exports.getSessionsByCardId = async function(req, res, next) {
  try {
    return status200(res, getSessionsByCardId(req));
  } catch (err) {
    sendError(err, next);
  }
};

async function getSessionsByCardId(req) {
  const { cardId } = req.params;
  return await db.session.find({ cardId });
}
