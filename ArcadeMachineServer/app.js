//DEPENDENCIES
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const salt = require("./src/handlers/salt");

const { gameRoutes, studentRoutes, sessionRoutes } = require("./src/routes");
const errorHandler = require("./src/handlers/errors");
const badRoute = require("./src/handlers/404Error");

const app = express();
const PORT = 8081;

// Allow cross-origin resource sharing
app.use(cors());

// Parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// Parse application/json
app.use(bodyParser.json());

// test download
app.get("/api/download", function(req, res) {
  var file = __dirname + "/test.txt";
  res.download(file); // Set disposition and send it.
});

app.use("/api/games/", gameRoutes);
app.use("/api/students/", studentRoutes);
app.use("/api/sessions/", sessionRoutes);
app.get("/api/salt/CCBD78DD529AC265B4D6BB4E668EB", salt); // will DELETE ALL FROM THE DATABASE, salt database with a new game, new users, and new sessions
app.use(badRoute);

app.use(errorHandler);


app.listen(PORT, function() {
  //eslint-disable-next-line
  console.log(`server started on port ${PORT}`);
});
