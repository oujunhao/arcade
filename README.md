# Arcade Cabinet

## How To Use

Download and extract `GameSystemServices.zip` from
[GitLab](https://gitlab.com/oujunhao/arcade/tree/development/GameSystemServices/GameSystemServices/GameSystemServices/bin)

Put them somewhere in your project folder, and add a reference to the .dll file.

1. Make sure they've been extracted
2. In the top bar on Visual Studio, click Project > Add Reference
3. On the left sidebar, click Browse
4. Click the bottom button that says `Browse...`
5. Navigate to the (extracted) folder with the `.dll` file
6. Select `GameSystemServices.dll`
7. Make sure the checkbox to the left is selected
8. Click `OK`

### `Form1.cs`

Import `GameSystemServices` after your `using` statements.

```cs
using GameSystemServices;
```

Register your game on [the admin panel](http://138.197.136.94:5000/newGame).

Click `GET TOKEN` and `COPY` it to your clipboard. Paste the code into your
`Form1` class so it looks like this:

```cs
namespace YourGameName
{
    public partial class Form1 : Form
    {
        const string gameToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJnYW1lSWQiOiI1YWRhMTRlYTQ5NWExZjA3YzkwZmI1NjAiLCJjYXJkSWQiOiIxIiwiaWf0IjoxNTI0MjQxNjQ4fQ.T7MXYXZnL8KUszaTCTJzQ4epTCB18LV_K076c0N9J7E"; // Dummy token that doesn't work
        public static Service service = new Service(Environment.GetCommandLineArgs(), gameToken);

        public Form1()
        {
            InitializeComponent();
        }
    }
}
```

### `GameScreen.cs`

When the game starts, call `startGame()` like this:

```cs
Form1.service.startGame();
```

When the game ends (player dies, time runs out), call `endGame()` like this:

```cs
Form1.service.endGame(score);
```

`score` is an `int` representing the player's score.

If you're not tracking highscores with your game, just call `endGame()`
without a score argument. We use this information to track how many people
are playing your game.

You can also find out if they got a personal highscore with the `bool`:

```cs
Form1.service.WasPersonalHighscore
```

To find out if they got the highest score of any player, use the `bool`:

```cs
Form1.service.WasGlobalHighscore
```

Use these last two `booleans` like this:

```cs
if (Form1.service.WasPersonalHighscore) {
  gotPersonalHighscoreLabel.Visible = true;
}
if (Form1.service.WasGlobalHighscore) {
  gotGlobalHighscoreLabel.Visible = true;
}
```

### `HighscoreScreen.cs`

Make a variable to hold list of highscores. Declare it in your highscore screen
so that it will be updated every time it is visited.

```cs
private void HighscoreScreen_Load(object sender, EventArgs e)
    {
        IList<GameSystemServices.Highscore> highscoreList = Form1.service.getHighscores();
    }
```

`getHighscores()` returns a list of the top 5 highscores. If you want more
results, specify the limit with `getHighscores(limit: 10)`. You can also skip
results with `getHighscores(skip: 10)`.

This list is made of `GameSystemServices.Highscore` objects that have three
properties:

- `Rank` - an `int` that is their position in the rankings
- `Name` - a `string` for their student name
- `Score` - an `int` that represents their score

## Design theme

| Fonts        |                                 |
| ------------ | ------------------------------- |
| Titles       | Endless Boss Battle all caps    |
| Descriptions | Endless Boss Battle normal case |

| Colors           | Hex Value | RGB values   |
| ---------------- | --------- | ------------ |
| Dark background  | #9e1906   | (158,25,6)   |
| Light background | #b8321b   | (184,50,27)  |
| Graphic details  | #c54e27   | (197,78,39)  |
| Secondary text   | #d26a29   | (210,106,41) |
| Primary text     | #f09328   | (240,147,40) |

## Todo

### Backend

DigitalOcean IP: 138.197.136.94

The database is not open to any computer except localhost. If you want to
connect to the cloud instance, forward your port 27017 to the server's:

```bash
ssh -f root@138.197.136.94 -L 27017:138.197.136.94:27017 -N
```

* [x] Send student by cardId
* [x] Register new game
* [x] Get game list
* [X] Feature games
* [X] Get list of featured games
* [x] Insert session

### Frontend

- [X] Load images
- [X] New users create profile
- [X] Play sounds for navigation
- [X] Offline error handling

#### API

* [x] new GameSystemServices(String gameId, String cardId)
  * [x] gameStart() // Call automatically
    * [x] Log starting time of game
  * [x] gameEnd(int score) // Call when closing
    * [x] Send session to server
  * [x] getHighscores(int bestPlace = 1, int worstPlace = 5)
    * [x] Send list of sessions
* [x] Test with simple game

## Data

### GameList Schema

```js
Gamelist = [
  {
    _id: ObjectId,
    name: string,
    publisher: string,
    date: instanceOf(Date),
    newUntil: instanceOf(Date),
    description: string,
    featured: bool,
    previewImagePath: string,
    exePath: string
  }
];
```

### Sessions Schema

```js
sessions = [
  {
    cardId: string,
    gameId: string,
    startTime: instanceOf(Date),
    endTime: instanceOf(Date),
    score: number
  }
];
```

### Student Schema

```js
students = [
  {
    name: string, // Student's full name
    cardId: string // Student card barcode
  }
];
```

### Game routes

#### `/api/games/`

* `get` returns all games
* `post` Creates a game within the database with a given _name, publisher,
  newUntil, featured, description, previewImagePath, exePath_

| Command            | Description                                  | Type   |
| ------------------ | -------------------------------------------- | ------ |
| `name`             | Name of a given game                         | string |
| `publisher`        | Name of publishers of a game                 | string |
| `newUntil`         | What date the game will stop being new       | Date   |
| `featured`         | If the game is featured                      | bool   |
| `description`      | Description of a given game                  | string |
| `previewImagePath` | Path on each local machine to a game's image | string |
| `exePath`          | Path of the exe of the game                  | string |

#### `/api/games/:gameId`

* `get` returns all game info pertaining to a given game
* `put` Updates a given game with new values
* `delete` Removes a given game and all of its data from a database

#### `api/games/:gameId/:cardId`

* `get` called by the DLL when entering a game.
* Returns a JWT with game info

### Highscore Routes

#### `/api/games/:gameToken/scores/:skip/:limit`

* `GET` Returns a sorted descending list of scores of a given game

### Session Routes

#### `/api/sessions/startSession`

* `post` starts a game session

#### `/api/sessions/endSession`

* `post` ends a game session

#### `/api/sessions/:cardId`

* `get` returns all sessions from a given player

### Student routes

#### `/api/students/:cardId`

* `get` returns the name of a given player
