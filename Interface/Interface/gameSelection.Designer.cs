﻿namespace Interface
{
    partial class GameSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameSelection));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.gameLabelPlus2 = new System.Windows.Forms.Label();
            this.gameLabelPlus1 = new System.Windows.Forms.Label();
            this.gameLabelSelected = new System.Windows.Forms.Label();
            this.gameLabelMinus1 = new System.Windows.Forms.Label();
            this.gameLabelMinus2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.Panel();
            this.isNewLabel = new System.Windows.Forms.Label();
            this.gameLabelTitle = new System.Windows.Forms.Label();
            this.featuredAndPlaysLabel = new System.Windows.Forms.Panel();
            this.playsLabel = new System.Windows.Forms.Label();
            this.featuredLabel = new System.Windows.Forms.Label();
            this.welcomeLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.Panel();
            this.gameLabelDescription = new System.Windows.Forms.Label();
            this.gameLabelPublisher = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.Panel();
            this.gameInProgressLabel = new System.Windows.Forms.Label();
            this.gamePictureBox = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.gameSelectionTimer = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.featuredAndPlaysLabel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gamePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(50)))), ((int)(((byte)(27)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.gameLabelPlus2);
            this.panel1.Controls.Add(this.gameLabelPlus1);
            this.panel1.Controls.Add(this.gameLabelSelected);
            this.panel1.Controls.Add(this.gameLabelMinus1);
            this.panel1.Controls.Add(this.gameLabelMinus2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(403, 1074);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Endless Boss Battle", 20F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.label1.Location = new System.Drawing.Point(0, 930);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(403, 144);
            this.label1.TabIndex = 7;
            this.label1.Text = "(PRESS BLACK TO LOG OUT)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gameLabelPlus2
            // 
            this.gameLabelPlus2.Dock = System.Windows.Forms.DockStyle.Top;
            this.gameLabelPlus2.Font = new System.Drawing.Font("Endless Boss Battle", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameLabelPlus2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.gameLabelPlus2.Location = new System.Drawing.Point(0, 739);
            this.gameLabelPlus2.Name = "gameLabelPlus2";
            this.gameLabelPlus2.Size = new System.Drawing.Size(403, 135);
            this.gameLabelPlus2.TabIndex = 5;
            this.gameLabelPlus2.Text = "G A M E (+2)";
            this.gameLabelPlus2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gameLabelPlus1
            // 
            this.gameLabelPlus1.Dock = System.Windows.Forms.DockStyle.Top;
            this.gameLabelPlus1.Font = new System.Drawing.Font("Endless Boss Battle", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameLabelPlus1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.gameLabelPlus1.Location = new System.Drawing.Point(0, 604);
            this.gameLabelPlus1.Name = "gameLabelPlus1";
            this.gameLabelPlus1.Size = new System.Drawing.Size(403, 135);
            this.gameLabelPlus1.TabIndex = 4;
            this.gameLabelPlus1.Text = "G A M E (+1)";
            this.gameLabelPlus1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gameLabelSelected
            // 
            this.gameLabelSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.gameLabelSelected.Dock = System.Windows.Forms.DockStyle.Top;
            this.gameLabelSelected.Font = new System.Drawing.Font("Endless Boss Battle", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameLabelSelected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.gameLabelSelected.Location = new System.Drawing.Point(0, 469);
            this.gameLabelSelected.Name = "gameLabelSelected";
            this.gameLabelSelected.Size = new System.Drawing.Size(403, 135);
            this.gameLabelSelected.TabIndex = 3;
            this.gameLabelSelected.Text = "SELECTED";
            this.gameLabelSelected.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gameLabelMinus1
            // 
            this.gameLabelMinus1.Dock = System.Windows.Forms.DockStyle.Top;
            this.gameLabelMinus1.Font = new System.Drawing.Font("Endless Boss Battle", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameLabelMinus1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.gameLabelMinus1.Location = new System.Drawing.Point(0, 334);
            this.gameLabelMinus1.Name = "gameLabelMinus1";
            this.gameLabelMinus1.Size = new System.Drawing.Size(403, 135);
            this.gameLabelMinus1.TabIndex = 2;
            this.gameLabelMinus1.Text = "G A M E (-1)";
            this.gameLabelMinus1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gameLabelMinus2
            // 
            this.gameLabelMinus2.Dock = System.Windows.Forms.DockStyle.Top;
            this.gameLabelMinus2.Font = new System.Drawing.Font("Endless Boss Battle", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameLabelMinus2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.gameLabelMinus2.Location = new System.Drawing.Point(0, 199);
            this.gameLabelMinus2.Name = "gameLabelMinus2";
            this.gameLabelMinus2.Size = new System.Drawing.Size(403, 135);
            this.gameLabelMinus2.TabIndex = 1;
            this.gameLabelMinus2.Text = "G A M E (-2)";
            this.gameLabelMinus2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Endless Boss Battle", 54F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 0, 3, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(403, 199);
            this.label4.TabIndex = 0;
            this.label4.Text = "G A M E S";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox1.Controls.Add(this.isNewLabel);
            this.groupBox1.Controls.Add(this.gameLabelTitle);
            this.groupBox1.Controls.Add(this.featuredAndPlaysLabel);
            this.groupBox1.Controls.Add(this.welcomeLabel);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(403, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(877, 281);
            this.groupBox1.TabIndex = 1;
            // 
            // isNewLabel
            // 
            this.isNewLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.isNewLabel.Font = new System.Drawing.Font("Endless Boss Battle", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.isNewLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.isNewLabel.Location = new System.Drawing.Point(62, 30);
            this.isNewLabel.Name = "isNewLabel";
            this.isNewLabel.Size = new System.Drawing.Size(132, 42);
            this.isNewLabel.TabIndex = 3;
            this.isNewLabel.Text = "NEW";
            this.isNewLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gameLabelTitle
            // 
            this.gameLabelTitle.AutoEllipsis = true;
            this.gameLabelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.gameLabelTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gameLabelTitle.Font = new System.Drawing.Font("Endless Boss Battle", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameLabelTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.gameLabelTitle.Location = new System.Drawing.Point(0, 110);
            this.gameLabelTitle.Name = "gameLabelTitle";
            this.gameLabelTitle.Size = new System.Drawing.Size(877, 71);
            this.gameLabelTitle.TabIndex = 0;
            this.gameLabelTitle.Text = "R A M S   A R C A D E";
            this.gameLabelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // featuredAndPlaysLabel
            // 
            this.featuredAndPlaysLabel.Controls.Add(this.playsLabel);
            this.featuredAndPlaysLabel.Controls.Add(this.featuredLabel);
            this.featuredAndPlaysLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.featuredAndPlaysLabel.Location = new System.Drawing.Point(0, 181);
            this.featuredAndPlaysLabel.Name = "featuredAndPlaysLabel";
            this.featuredAndPlaysLabel.Size = new System.Drawing.Size(877, 100);
            this.featuredAndPlaysLabel.TabIndex = 4;
            // 
            // playsLabel
            // 
            this.playsLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.playsLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playsLabel.Font = new System.Drawing.Font("Endless Boss Battle", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playsLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.playsLabel.Location = new System.Drawing.Point(0, 0);
            this.playsLabel.Name = "playsLabel";
            this.playsLabel.Size = new System.Drawing.Size(474, 100);
            this.playsLabel.TabIndex = 4;
            this.playsLabel.Text = "0 PLAYS";
            this.playsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // featuredLabel
            // 
            this.featuredLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.featuredLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.featuredLabel.Font = new System.Drawing.Font("Endless Boss Battle", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.featuredLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.featuredLabel.Location = new System.Drawing.Point(474, 0);
            this.featuredLabel.Name = "featuredLabel";
            this.featuredLabel.Size = new System.Drawing.Size(403, 100);
            this.featuredLabel.TabIndex = 5;
            this.featuredLabel.Text = "FEATURED";
            this.featuredLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // welcomeLabel
            // 
            this.welcomeLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.welcomeLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.welcomeLabel.Font = new System.Drawing.Font("Endless Boss Battle", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.welcomeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.welcomeLabel.Location = new System.Drawing.Point(0, 0);
            this.welcomeLabel.Name = "welcomeLabel";
            this.welcomeLabel.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.welcomeLabel.Size = new System.Drawing.Size(877, 110);
            this.welcomeLabel.TabIndex = 2;
            this.welcomeLabel.Text = "WELCOME PERSON";
            this.welcomeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gameLabelDescription);
            this.groupBox2.Controls.Add(this.gameLabelPublisher);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(403, 739);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(877, 335);
            this.groupBox2.TabIndex = 2;
            // 
            // gameLabelDescription
            // 
            this.gameLabelDescription.AutoEllipsis = true;
            this.gameLabelDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.gameLabelDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gameLabelDescription.Font = new System.Drawing.Font("Endless Boss Battle", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameLabelDescription.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.gameLabelDescription.Location = new System.Drawing.Point(0, 68);
            this.gameLabelDescription.Name = "gameLabelDescription";
            this.gameLabelDescription.Padding = new System.Windows.Forms.Padding(64, 16, 64, 54);
            this.gameLabelDescription.Size = new System.Drawing.Size(877, 267);
            this.gameLabelDescription.TabIndex = 1;
            this.gameLabelDescription.Text = resources.GetString("gameLabelDescription.Text");
            // 
            // gameLabelPublisher
            // 
            this.gameLabelPublisher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.gameLabelPublisher.Dock = System.Windows.Forms.DockStyle.Top;
            this.gameLabelPublisher.Font = new System.Drawing.Font("Endless Boss Battle", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameLabelPublisher.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.gameLabelPublisher.Location = new System.Drawing.Point(0, 0);
            this.gameLabelPublisher.Name = "gameLabelPublisher";
            this.gameLabelPublisher.Padding = new System.Windows.Forms.Padding(0, 0, 64, 0);
            this.gameLabelPublisher.Size = new System.Drawing.Size(877, 68);
            this.gameLabelPublisher.TabIndex = 0;
            this.gameLabelPublisher.Text = "Aaron and Cameron";
            this.gameLabelPublisher.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.gameInProgressLabel);
            this.groupBox4.Controls.Add(this.gamePictureBox);
            this.groupBox4.Controls.Add(this.panel2);
            this.groupBox4.Controls.Add(this.panel3);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(403, 281);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(877, 458);
            this.groupBox4.TabIndex = 4;
            // 
            // gameInProgressLabel
            // 
            this.gameInProgressLabel.AutoEllipsis = true;
            this.gameInProgressLabel.BackColor = System.Drawing.Color.Transparent;
            this.gameInProgressLabel.Font = new System.Drawing.Font("Endless Boss Battle", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameInProgressLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.gameInProgressLabel.Location = new System.Drawing.Point(0, 151);
            this.gameInProgressLabel.Name = "gameInProgressLabel";
            this.gameInProgressLabel.Size = new System.Drawing.Size(877, 193);
            this.gameInProgressLabel.TabIndex = 3;
            this.gameInProgressLabel.Text = "GAME IN PROGRESS";
            this.gameInProgressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.gameInProgressLabel.Visible = false;
            // 
            // gamePictureBox
            // 
            this.gamePictureBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.gamePictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gamePictureBox.ErrorImage = ((System.Drawing.Image)(resources.GetObject("gamePictureBox.ErrorImage")));
            this.gamePictureBox.Image = ((System.Drawing.Image)(resources.GetObject("gamePictureBox.Image")));
            this.gamePictureBox.Location = new System.Drawing.Point(60, 0);
            this.gamePictureBox.Margin = new System.Windows.Forms.Padding(128, 0, 0, 0);
            this.gamePictureBox.Name = "gamePictureBox";
            this.gamePictureBox.Size = new System.Drawing.Size(757, 458);
            this.gamePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.gamePictureBox.TabIndex = 0;
            this.gamePictureBox.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(60, 458);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(817, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(60, 458);
            this.panel3.TabIndex = 2;
            // 
            // gameSelectionTimer
            // 
            this.gameSelectionTimer.Enabled = true;
            this.gameSelectionTimer.Interval = 1000;
            this.gameSelectionTimer.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // GameSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "GameSelection";
            this.Size = new System.Drawing.Size(1280, 1074);
            this.Load += new System.EventHandler(this.GameSelection_Load);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.GameSelection_PreviewKeyDown);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.featuredAndPlaysLabel.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gamePictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel groupBox1;
        private System.Windows.Forms.Panel groupBox2;
        private System.Windows.Forms.Label gameLabelDescription;
        private System.Windows.Forms.Label gameLabelPublisher;
        private System.Windows.Forms.Panel groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label gameLabelTitle;
        private System.Windows.Forms.PictureBox gamePictureBox;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.Label gameLabelPlus2;
        private System.Windows.Forms.Label gameLabelPlus1;
        private System.Windows.Forms.Label gameLabelSelected;
        private System.Windows.Forms.Label gameLabelMinus1;
        private System.Windows.Forms.Label gameLabelMinus2;
        private System.Windows.Forms.Timer gameSelectionTimer;
        private System.Windows.Forms.Label welcomeLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label isNewLabel;
        private System.Windows.Forms.Panel featuredAndPlaysLabel;
        private System.Windows.Forms.Label featuredLabel;
        private System.Windows.Forms.Label playsLabel;
        private System.Windows.Forms.Label gameInProgressLabel;
    }
}
