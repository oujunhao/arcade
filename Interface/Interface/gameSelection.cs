﻿using System;
using Interface.Properties;
using static Interface.MainMenu;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Media;

namespace Interface
{
    public partial class GameSelection : UserControl
    {
        const string FEATURED_ICON = "⭐";
        readonly TimeSpan TIMEOUT_DURATION = new TimeSpan(0, 0, 30);

        Games games = new Games();
        MediaPlayer selectedGameCursorMoved;
        MediaPlayer gameLaunched;

        DateTime lastInteractiveTime = DateTime.Now;

        public GameSelection()
        {
            InitializeComponent();
            gameSelectionTimer.Start();
            showCurrentPlayerNameWelcome();
            initializeSounds();
            updateState();
        }

        private void GameSelection_Load(object sender, EventArgs e)
        {
            games.loadGamesFromServer().ContinueWith((_) => this.Invoke((MethodInvoker)delegate { updateState(); }));
        }

        private void showCurrentPlayerNameWelcome() => welcomeLabel.Text = interleaveSpaces("Welcome " + currentPlayerName);

        private void initializeSounds()
        {
            selectedGameCursorMoved = new MediaPlayer();
            selectedGameCursorMoved.Open(new Uri(Application.StartupPath + @"\Resources\move_selection.wav"));
            gameLaunched = new MediaPlayer();
            gameLaunched.Open(new Uri(Application.StartupPath + @"\Resources\launch_game.wav"));
        }

        public void updateState()
        {
            updateGamePublisher();
            updateGameTitle();
            updateGameImage();
            updateGameLabelPositions();
            updateGameDescription();
            updateFeaturedTag();
            updatePlayCount();
            updateIsNewLabel();
            Refresh();
        }

        private void updateGamePublisher() => gameLabelPublisher.Text = games.getSelectedGame().publisher.ToUpper();

        private void updateGameTitle() => gameLabelTitle.Text = interleaveSpaces(games.getSelectedGame().name.ToUpper());

        private void updateGameImage() => gamePictureBox.ImageLocation = games.getSelectedGame().previewImagePath;

        private void updateGameLabelPositions()
        {
            gameLabelMinus2.Text = games.getSelectedGame(-2).nameWithFeatured(icon: FEATURED_ICON).ToUpper();
            gameLabelMinus1.Text = games.getSelectedGame(-1).nameWithFeatured(icon: FEATURED_ICON).ToUpper();
            gameLabelSelected.Text = games.getSelectedGame().nameWithFeatured(icon: FEATURED_ICON).ToUpper();
            gameLabelPlus1.Text = games.getSelectedGame(1).nameWithFeatured(icon: FEATURED_ICON).ToUpper();
            gameLabelPlus2.Text = games.getSelectedGame(2).nameWithFeatured(icon: FEATURED_ICON).ToUpper();
        }

        private void updateGameDescription() => gameLabelDescription.Text = games.getSelectedGame().description;

        private void updateFeaturedTag() => featuredLabel.Visible = (bool)games.getSelectedGame().featured;

        private void updatePlayCount() => playsLabel.Text = $"{games.getSelectedGame().numberPlays} PLAYS";

        private void updateIsNewLabel() => isNewLabel.Visible = games.getSelectedGame().isNew;

        private void GameSelection_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e) => handleNavigationKeypress(e);

        private void handleNavigationKeypress(PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    games.moveSelection(-1);
                    selectedGameCursorMoved.Play();
                    break;
                case Keys.Down:
                    games.moveSelection(1);
                    selectedGameCursorMoved.Play();
                    break;
                case Keys.Space:
                    if (!games.isGameRunning())
                    {
                        gameLaunched.Play();
                        games.playSelectedGame();
                        gameInProgressLabel.Visible = true;
                        if (!games.isGameRunning()) gameInProgressLabel.Visible = false;
                        else
                            games.currentProcess.Exited += (_, __) => this.Invoke((MethodInvoker)delegate { gameInProgressLabel.Visible = false; });
                    }
                    break;
                case Keys.Escape:
                    if (games.killCurrentGame()) break;
                    logOutAndGoToMenu();
                    break;
                default: break;
            }
            initializeSounds();
            updateLastInteractiveTime();
            updateState();
        }

        private void updateLastInteractiveTime() => lastInteractiveTime = DateTime.Now;

        private void logOutAndGoToMenu()
        {
            currentPlayerName = null;
            cardId = null;
            gameSelectionTimer.Stop();
            goToControl(new Main(), this);
        }

        public static string interleaveSpaces(string name) => String.Join(" ", name.ToCharArray());

        private void timer1_Tick(object sender, EventArgs e) => logOutIfUnattended();

        private void logOutIfUnattended()
        {
            if (games.isGameRunning())
                lastInteractiveTime = DateTime.Now;

            TimeSpan timeElapsedSinceInteractive = DateTime.Now - lastInteractiveTime;
            if (timeElapsedSinceInteractive >= TIMEOUT_DURATION) logOutAndGoToMenu();
        }
    }
}
