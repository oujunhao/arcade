﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public class Game
    {
        public string _id { get; set; }
        public string name { get; set; }
        public string publisher { get; set; }
        public DateTime date { get; set; }
        public DateTime? newUntil { get; set; }
        public bool isNew { get; set; }
        public int numberPlays { get; set; }
        public bool? featured { get; set; }
        public string description { get; set; }
        public string previewImagePath { get; set; }
        public string exePath { get; set; }

        public string nameWithFeatured(string icon, string space = " ") => name + ((bool)featured ? space + icon : "");
    }
}
