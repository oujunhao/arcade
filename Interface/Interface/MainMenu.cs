﻿using Interface.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Net.Http;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Interface
{
    public partial class MainMenu : Form
    {
        public const int DIGITS_IN_STUDENT_CARD = 9;
        public static string PORT = "8081"; // Default port that the NodeJS API is running on, set for testing
        public static string SERVER_IP = "138.197.136.94"; // Default IP that the NodeJS API is running on, set for testing

        public static string cardId;
        public static string currentPlayerName;
        public static HttpClient httpClient = new HttpClient();

        public MainMenu()
        {
            InitializeComponent();
            Cursor.Hide();
            GoFullScreen(true);
            DoubleBuffered = true;

            //start the program centred on the Menu Screen
            Main ms = new Main();
            this.Controls.Add(ms);
            ms.Location = new Point((Screen.PrimaryScreen.Bounds.Width - ms.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - ms.Height) / 2);
            loadServerConfig();
        }

        private void loadServerConfig()
        {
            string path = Application.StartupPath + @"\config.txt";

            if (!File.Exists(path))
                return;

            string readText = File.ReadAllText(path);
            SERVER_IP = readText.Trim();
        }

        // Go full screen to be optimized for arcade
        void GoFullScreen(bool fullscreen)
        {
            if (fullscreen)
            {
                this.FormBorderStyle = FormBorderStyle.None;
                this.Bounds = Screen.PrimaryScreen.Bounds;
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = FormBorderStyle.Sizable;
            }
        }

        public static void goToControl(UserControl control, UserControl oldControl)
        {
            Form form = oldControl.FindForm();

            if (form != null)
            {
                form.Controls.Add(control);
                form.Controls.Remove(oldControl);

                control.Location = new Point((form.Width - control.Width) / 2, (form.Height - control.Height) / 2);
            }
        }
    }
}
