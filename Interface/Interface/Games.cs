﻿using System;
using static Interface.MainMenu;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameSystemServices;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace Interface
{
    public class Games
    {

        private List<Game> games = new List<Game>();
        private int selectedGameIndex = 0;
        public Process currentProcess;
        public bool serverConnection = false;
        public bool isGameRunning() => currentProcess != null ? !currentProcess.HasExited : false;


        public async Task loadGamesFromServer()
        {
            try
            {
                string url = $"http://{SERVER_IP}:{PORT}/api/games";
                string responseString = await httpClient.GetStringAsync(url);
                JObject response = JObject.Parse(responseString);
                IList<JToken> serialHighscores = response["foundGames"].Children().ToList();
                List<Game> loadedGames = serialHighscores.Select(
                    game => game.ToObject<Game>())
                    .ToList<Game>();
                games.Clear();
                games = loadedGames;
                serverConnection = true;
            }
            catch (HttpRequestException)
            {
                serverConnection = false;
            }
        }

        public void loadDummyGames()
        {
            for (int i = 1; i <= 10; i++)
            {
                Game newGame = new Game
                {
                    name = $"Game {i}",
                    publisher = i % 2 == 0 ? "Aaron Olsen" : "Cameron Teasdale",
                    description = i % 2 == 0 ? "Chfsd sjglkahj  slkdjf lashglija gh sg asdfglka " : "ahskf hsd fjakshfhfd hhf shfahfsh ahfsh fshfah df",
                    featured = false
                };
                games.Add(newGame);
            }
        }

        public void moveSelection(int difference) => selectedGameIndex = wrappedIndex(selectedGameIndex + difference);

        public Game getSelectedGame(int offset = 0) => games.Count > 0 ? games[wrappedIndex(selectedGameIndex + offset)] : new Game { name = "Loading...", publisher = "Loading...", description = "Loading...", featured = false };

        private int wrappedIndex(int index) => mod(index, games.Count);

        public static int mod(int x, int m) => m != 0 ? (x % m + m) % m : 0;

        public void playSelectedGame()
        {
            if (serverConnection)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    UseShellExecute = false,
                    Arguments = $"{getSelectedGameToken()} {SERVER_IP} {PORT}",
                    FileName = Application.StartupPath + getSelectedGame().exePath
                };
                try
                {
                    currentProcess = Process.Start(startInfo);
                    currentProcess.EnableRaisingEvents = true;
                }
                catch (System.ComponentModel.Win32Exception)
                {
                    currentProcess = null;
                }
            }
        }

        public bool killCurrentGame()
        {
            try
            {
                currentProcess.Kill();
                return true;
            }
            catch { }
            return false;
        }

        private string getSelectedGameToken()
        {
            try
            {
                string url = $"http://{SERVER_IP}:{PORT}/api/games/enterGame/{getSelectedGame()._id}/{cardId}";
                var responseString = httpClient.GetStringAsync(url);
                JObject response = JObject.Parse(responseString.Result);
                return response["gameToken"].ToString();
            }
            catch {
                return "404";
            }
        }
    }
}
