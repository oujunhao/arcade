﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Net.Http;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Interface
{
    public partial class SignIn : Form
    {
        const string SERVER_IP = "138.197.136.94";
        const string PORT = "8081";
        HttpClient httpClient = new HttpClient();
        IList<Game> games;

        public SignIn()
        {
            InitializeComponent();
            cardInput.Width = 0;
            cardInput.Height = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private IList<Game> GetGameList()
        {
            try
            {
                var responseString = httpClient
                    .GetStringAsync($"http://{SERVER_IP}:{PORT}/api/games");
                JObject response = JObject.Parse(responseString.Result);
                IList<JToken> serialGames = response["foundGames"].Children().ToList();
                IList<Game> games = serialGames.Select(
                    game => game.ToObject<Game>())
                    .ToList<Game>();
                return games;
            }
            catch (Exception error)
            {
                throw error;
            }
        }

        private string getNameResponse(string cardId)
        {
            try
            {
                pictureBox1.Visible = false;
                var responseString = httpClient
                    .GetStringAsync($"http://{SERVER_IP}:{PORT}/api/students/{cardId}");
                JObject response = JObject.Parse(responseString.Result);
                string greetingName = "Welcome " + response["student"]["name"].ToString();
                return greetingName;
            }
            catch (System.InvalidOperationException)
            {
                return "Please enter a valid student card";
            }
        }

        private void PleaseScanLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
