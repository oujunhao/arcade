﻿using System;
using static Interface.MainMenu;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace Interface.Properties
{
    public partial class Main : UserControl
    {
        readonly TimeSpan ERROR_MESSAGE_DURATION = new TimeSpan(0, 0, 4);
        const string defaultMessage = "Scan Student Card Or Press Green Button";

        string scheduledMessage = defaultMessage;
        double messageRed = 197;
        double messageGreen = 78;
        double messageBlue = 39;
        bool decreasingOpacity = true;

        public Main() => InitializeComponent();

        private void Main_Load(object sender, EventArgs e) => initializeCardInput();

        private void initializeCardInput()
        {
            cardInput.Width = 0;
            cardInput.Height = 0;
            cardInput.Focus();
        }

        private void timer1_Tick(object sender, EventArgs e) => changeMessageOpacity();

        private void CheckKeys(object sender, KeyPressEventArgs e)
        {
            switch ((Keys)e.KeyChar)
            {
                case Keys.Space:
                    cardInput.Text = "theIgnoredGuestCardId";
                    goto case Keys.Enter;
                case Keys.Enter:
                    cardId = cardInput.Text;
                    getNameResponseAndLogin();
                    e.Handled = true;
                    break;
            }
        }

        public void changeMessageOpacity()
        {
            if (messageRed >= 197)
                decreasingOpacity = true;
            else if (messageRed <= 158)
            {
                decreasingOpacity = false;
                PleaseScanLabel.Text = scheduledMessage;
            }

            if (decreasingOpacity)
                decreaseOpacity();
            else if (!decreasingOpacity)
                increaseOpacity();

            PleaseScanLabel.ForeColor = Color.FromArgb(255, (int)messageRed, (int)messageGreen, (int)messageBlue);
        }

        public void decreaseOpacity()
        {
            messageRed -= 0.69;
            messageGreen -= 0.8833;
            messageBlue -= 0.55;
        }

        public void increaseOpacity()
        {
            messageRed += 0.69;
            messageGreen += 0.8833;
            messageBlue += 0.55;
        }

        private async void getNameResponseAndLogin()
        {
            if (cardInput.Text == "") return;
            try
            {
                scheduledMessage = "Loading...";
                string url = $"http://{SERVER_IP}:{PORT}/api/students/{cardId}";
                using (var httpClient = new HttpClient()) {
                    var responseString = await httpClient.GetStringAsync(url);
                    JObject response = JObject.Parse(responseString);
                    string greetingName = response["student"]["name"].ToString();

                    currentPlayerName = greetingName;
                }
            }
            catch (HttpRequestException) { }
            catch (System.InvalidOperationException)
            {
                if (cardId.Length == DIGITS_IN_STUDENT_CARD)
                    goToControl(new newName(), this);
                else
                    showInvalidError("Invalid Student Card");
            }
            catch (System.AggregateException)
            {
                showInvalidError("No Connection To Game Server");
            }
            if (currentPlayerName != null)
                goToControl(new GameSelection(), this);
        }

        public async void showInvalidError(string message)
        {
            scheduledMessage = message;
            cardInput.Text = "";
            cardInput.Visible = false;
            await Task.Delay(ERROR_MESSAGE_DURATION);
            scheduledMessage = defaultMessage;
            cardInput.Visible = true;
            cardInput.Focus();
        }

    }
}
