﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface.Properties
{
    public partial class rejectionDialog : Form
    {
        private static rejectionDialog rejectionForm;
        private static DialogResult buttonResult = new DialogResult();

        public rejectionDialog()
        {
            InitializeComponent();
        }

        public static DialogResult Show(string name)
        {

            rejectionForm = new rejectionDialog();
            rejectionForm.nameLabel.Text = name;
            rejectionForm.ShowDialog();
            return buttonResult;
        }
        private void rejectionDialog_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Space:
                    selectOption();
                    break;
                case Keys.Escape:
                    selectOption();
                    break;
            }
        }

        private void selectOption()
        {
            buttonResult = DialogResult.OK;
            rejectionForm.Close();
        }
    }
}
