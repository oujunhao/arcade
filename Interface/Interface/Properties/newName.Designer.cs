﻿namespace Interface.Properties
{
    partial class newName
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nameTitleLabel = new System.Windows.Forms.Label();
            this.subtitleTextLabel = new System.Windows.Forms.Label();
            this.letterOneLabel = new System.Windows.Forms.Label();
            this.letterTwoLabel = new System.Windows.Forms.Label();
            this.letterThreeLabel = new System.Windows.Forms.Label();
            this.letterFourLabel = new System.Windows.Forms.Label();
            this.letterFiveLabel = new System.Windows.Forms.Label();
            this.highlightCharacterTimer = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nameTitleLabel
            // 
            this.nameTitleLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.nameTitleLabel.Font = new System.Drawing.Font("Endless Boss Battle", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameTitleLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.nameTitleLabel.Location = new System.Drawing.Point(0, 0);
            this.nameTitleLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nameTitleLabel.Name = "nameTitleLabel";
            this.nameTitleLabel.Padding = new System.Windows.Forms.Padding(0, 0, 0, 16);
            this.nameTitleLabel.Size = new System.Drawing.Size(1000, 108);
            this.nameTitleLabel.TabIndex = 0;
            this.nameTitleLabel.Text = "Sign Up";
            this.nameTitleLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // subtitleTextLabel
            // 
            this.subtitleTextLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.subtitleTextLabel.Font = new System.Drawing.Font("Endless Boss Battle", 22.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleTextLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.subtitleTextLabel.Location = new System.Drawing.Point(0, 108);
            this.subtitleTextLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.subtitleTextLabel.Name = "subtitleTextLabel";
            this.subtitleTextLabel.Size = new System.Drawing.Size(1000, 44);
            this.subtitleTextLabel.TabIndex = 1;
            this.subtitleTextLabel.Text = "Enter Your Name With The Joystick";
            this.subtitleTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // letterOneLabel
            // 
            this.letterOneLabel.Font = new System.Drawing.Font("Endless Boss Battle", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.letterOneLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.letterOneLabel.Location = new System.Drawing.Point(298, 269);
            this.letterOneLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.letterOneLabel.Name = "letterOneLabel";
            this.letterOneLabel.Padding = new System.Windows.Forms.Padding(1, 9, 0, 0);
            this.letterOneLabel.Size = new System.Drawing.Size(59, 66);
            this.letterOneLabel.TabIndex = 2;
            this.letterOneLabel.Text = "A";
            this.letterOneLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // letterTwoLabel
            // 
            this.letterTwoLabel.Font = new System.Drawing.Font("Endless Boss Battle", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.letterTwoLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.letterTwoLabel.Location = new System.Drawing.Point(383, 269);
            this.letterTwoLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.letterTwoLabel.Name = "letterTwoLabel";
            this.letterTwoLabel.Padding = new System.Windows.Forms.Padding(1, 9, 0, 0);
            this.letterTwoLabel.Size = new System.Drawing.Size(59, 66);
            this.letterTwoLabel.TabIndex = 3;
            this.letterTwoLabel.Text = "A";
            this.letterTwoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // letterThreeLabel
            // 
            this.letterThreeLabel.Font = new System.Drawing.Font("Endless Boss Battle", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.letterThreeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.letterThreeLabel.Location = new System.Drawing.Point(471, 269);
            this.letterThreeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.letterThreeLabel.Name = "letterThreeLabel";
            this.letterThreeLabel.Padding = new System.Windows.Forms.Padding(1, 9, 0, 0);
            this.letterThreeLabel.Size = new System.Drawing.Size(59, 66);
            this.letterThreeLabel.TabIndex = 4;
            this.letterThreeLabel.Text = "A";
            this.letterThreeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // letterFourLabel
            // 
            this.letterFourLabel.Font = new System.Drawing.Font("Endless Boss Battle", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.letterFourLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.letterFourLabel.Location = new System.Drawing.Point(563, 269);
            this.letterFourLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.letterFourLabel.Name = "letterFourLabel";
            this.letterFourLabel.Padding = new System.Windows.Forms.Padding(1, 9, 0, 0);
            this.letterFourLabel.Size = new System.Drawing.Size(59, 66);
            this.letterFourLabel.TabIndex = 5;
            this.letterFourLabel.Text = "A";
            this.letterFourLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // letterFiveLabel
            // 
            this.letterFiveLabel.Font = new System.Drawing.Font("Endless Boss Battle", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.letterFiveLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.letterFiveLabel.Location = new System.Drawing.Point(656, 269);
            this.letterFiveLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.letterFiveLabel.Name = "letterFiveLabel";
            this.letterFiveLabel.Padding = new System.Windows.Forms.Padding(1, 9, 0, 0);
            this.letterFiveLabel.Size = new System.Drawing.Size(59, 66);
            this.letterFiveLabel.TabIndex = 6;
            this.letterFiveLabel.Text = "A";
            this.letterFiveLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // highlightCharacterTimer
            // 
            this.highlightCharacterTimer.Enabled = true;
            this.highlightCharacterTimer.Interval = 500;
            this.highlightCharacterTimer.Tick += new System.EventHandler(this.highlightCharacterTimer_Tick);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Endless Boss Battle", 22.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.label1.Location = new System.Drawing.Point(0, 484);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1000, 78);
            this.label1.TabIndex = 7;
            this.label1.Text = "(Press Green Button To Confirm)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // newName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.Controls.Add(this.label1);
            this.Controls.Add(this.letterFiveLabel);
            this.Controls.Add(this.letterFourLabel);
            this.Controls.Add(this.letterThreeLabel);
            this.Controls.Add(this.letterTwoLabel);
            this.Controls.Add(this.letterOneLabel);
            this.Controls.Add(this.subtitleTextLabel);
            this.Controls.Add(this.nameTitleLabel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "newName";
            this.Size = new System.Drawing.Size(1000, 562);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.newName_PreviewKeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label nameTitleLabel;
        private System.Windows.Forms.Label subtitleTextLabel;
        private System.Windows.Forms.Label letterOneLabel;
        private System.Windows.Forms.Label letterTwoLabel;
        private System.Windows.Forms.Label letterThreeLabel;
        private System.Windows.Forms.Label letterFourLabel;
        private System.Windows.Forms.Label letterFiveLabel;
        private System.Windows.Forms.Timer highlightCharacterTimer;
        private System.Windows.Forms.Label label1;
    }
}
