﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface.Properties
{
    public partial class confirmationDialog : Form
    {
        private static confirmationDialog confirmationForm;
        private static DialogResult buttonResult = new DialogResult();

        bool confirm = false;
        public confirmationDialog()
        {
            InitializeComponent();
        }

        private void confirmationDialog_Load(object sender, EventArgs e)
        {

        }
        public static DialogResult Show(string name)
        {

            confirmationForm = new confirmationDialog();
            confirmationForm.nameLabel.Text = name;
            confirmationForm.ShowDialog();
            return buttonResult;
        }
        private void confirmationDialog_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Space:
                    selectOption();
                    break;
                case Keys.Right:
                    switchSelectedOption();
                    break;
                case Keys.Left:
                    switchSelectedOption();
                    break;
                case Keys.Escape:
                    confirm = false;
                    selectOption();
                    break;
            }
        }
        private void switchSelectedOption()
        {
            confirm = !confirm;

            changeLabel.ForeColor = confirm ? Color.FromArgb(255, 158, 25, 6) : Color.FromArgb(255, 240, 147, 40);
            confirmLabel.ForeColor = confirm ? Color.FromArgb(255, 240, 147, 40) : Color.FromArgb(255, 158, 25, 6);
        }

        private void selectOption()
        {
            if (confirm)
                buttonResult = DialogResult.OK;
            else
                buttonResult = DialogResult.Cancel;
            confirmationForm.Close();
        }
    }
}
