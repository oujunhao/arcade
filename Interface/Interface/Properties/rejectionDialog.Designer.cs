﻿namespace Interface.Properties
{
    partial class rejectionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.changeLabel = new System.Windows.Forms.Label();
            this.warningLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.confirmationMessageLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // changeLabel
            // 
            this.changeLabel.AutoSize = true;
            this.changeLabel.Font = new System.Drawing.Font("Endless Boss Battle", 22.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.changeLabel.Location = new System.Drawing.Point(232, 269);
            this.changeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.changeLabel.Name = "changeLabel";
            this.changeLabel.Size = new System.Drawing.Size(127, 30);
            this.changeLabel.TabIndex = 8;
            this.changeLabel.Text = "Change";
            // 
            // warningLabel
            // 
            this.warningLabel.AutoSize = true;
            this.warningLabel.Font = new System.Drawing.Font("Endless Boss Battle", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warningLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(78)))), ((int)(((byte)(39)))));
            this.warningLabel.Location = new System.Drawing.Point(176, 194);
            this.warningLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.warningLabel.Name = "warningLabel";
            this.warningLabel.Size = new System.Drawing.Size(224, 22);
            this.warningLabel.TabIndex = 7;
            this.warningLabel.Text = "That Name Is Illegal";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Endless Boss Battle", 28.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.nameLabel.Location = new System.Drawing.Point(230, 107);
            this.nameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(138, 38);
            this.nameLabel.TabIndex = 6;
            this.nameLabel.Text = "N A M E";
            // 
            // confirmationMessageLabel
            // 
            this.confirmationMessageLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.confirmationMessageLabel.Font = new System.Drawing.Font("Endless Boss Battle", 19.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmationMessageLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.confirmationMessageLabel.Location = new System.Drawing.Point(0, 0);
            this.confirmationMessageLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.confirmationMessageLabel.Name = "confirmationMessageLabel";
            this.confirmationMessageLabel.Size = new System.Drawing.Size(605, 107);
            this.confirmationMessageLabel.TabIndex = 5;
            this.confirmationMessageLabel.Text = "Y U  SO STUPID";
            this.confirmationMessageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rejectionDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(50)))), ((int)(((byte)(27)))));
            this.ClientSize = new System.Drawing.Size(605, 322);
            this.Controls.Add(this.changeLabel);
            this.Controls.Add(this.warningLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.confirmationMessageLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "rejectionDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.rejectionDialog_PreviewKeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label changeLabel;
        private System.Windows.Forms.Label warningLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label confirmationMessageLabel;
    }
}