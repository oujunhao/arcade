﻿namespace Interface.Properties
{
    partial class confirmationDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.confirmLabel = new System.Windows.Forms.Label();
            this.changeLabel = new System.Windows.Forms.Label();
            this.warningLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.confirmationMessageLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // confirmLabel
            // 
            this.confirmLabel.AutoSize = true;
            this.confirmLabel.Font = new System.Drawing.Font("Endless Boss Battle", 22.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.confirmLabel.Location = new System.Drawing.Point(637, 515);
            this.confirmLabel.Name = "confirmLabel";
            this.confirmLabel.Size = new System.Drawing.Size(268, 59);
            this.confirmLabel.TabIndex = 9;
            this.confirmLabel.Text = "Confirm";
            // 
            // changeLabel
            // 
            this.changeLabel.AutoSize = true;
            this.changeLabel.Font = new System.Drawing.Font("Endless Boss Battle", 22.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.changeLabel.Location = new System.Drawing.Point(263, 515);
            this.changeLabel.Name = "changeLabel";
            this.changeLabel.Size = new System.Drawing.Size(253, 59);
            this.changeLabel.TabIndex = 8;
            this.changeLabel.Text = "Change";
            // 
            // warningLabel
            // 
            this.warningLabel.AutoSize = true;
            this.warningLabel.Font = new System.Drawing.Font("Endless Boss Battle", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warningLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(78)))), ((int)(((byte)(39)))));
            this.warningLabel.Location = new System.Drawing.Point(249, 373);
            this.warningLabel.Name = "warningLabel";
            this.warningLabel.Size = new System.Drawing.Size(673, 43);
            this.warningLabel.TabIndex = 7;
            this.warningLabel.Text = "This Cannot Be Changed Later";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Endless Boss Battle", 28.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(147)))), ((int)(((byte)(40)))));
            this.nameLabel.Location = new System.Drawing.Point(459, 205);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(273, 75);
            this.nameLabel.TabIndex = 6;
            this.nameLabel.Text = "N A M E";
            // 
            // confirmationMessageLabel
            // 
            this.confirmationMessageLabel.AutoSize = true;
            this.confirmationMessageLabel.Font = new System.Drawing.Font("Endless Boss Battle", 19.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmationMessageLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(106)))), ((int)(((byte)(41)))));
            this.confirmationMessageLabel.Location = new System.Drawing.Point(65, 37);
            this.confirmationMessageLabel.Name = "confirmationMessageLabel";
            this.confirmationMessageLabel.Size = new System.Drawing.Size(1111, 53);
            this.confirmationMessageLabel.TabIndex = 5;
            this.confirmationMessageLabel.Text = "Are You Sure You Want To Name Yourself";
            // 
            // confirmationDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(50)))), ((int)(((byte)(27)))));
            this.ClientSize = new System.Drawing.Size(1210, 620);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Controls.Add(this.confirmLabel);
            this.Controls.Add(this.changeLabel);
            this.Controls.Add(this.warningLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.confirmationMessageLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "confirmationDialog";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.confirmationDialog_Load);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.confirmationDialog_PreviewKeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label confirmLabel;
        private System.Windows.Forms.Label changeLabel;
        private System.Windows.Forms.Label warningLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label confirmationMessageLabel;
    }
}