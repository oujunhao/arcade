﻿using System;
using static Interface.MainMenu;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.IO;

namespace Interface.Properties
{
    public partial class newName : UserControl
    {
        char[] name = new char[] { (char)65, (char)65, (char)65, (char)65, (char)65 };
        List<Label> characters = new List<Label>();
        List<String> badWords = new List<String>();


        int currentcharacter = 0;
        bool canInput = true;

        public newName()
        {
            InitializeComponent();
            characters.Add(letterOneLabel);
            characters.Add(letterTwoLabel);
            characters.Add(letterThreeLabel);
            characters.Add(letterFourLabel);
            characters.Add(letterFiveLabel);
            loadBadWords();
        }

        private void loadBadWords()
        {
            string path = Application.StartupPath + @"\badWords\fiveCharBadWords.txt";

            if (!File.Exists(path))
                return;

            List<String> readLines = File.ReadLines(path).ToList<String>();
            badWords = readLines;
        }

        private void newName_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (canInput)
            {
                switch (e.KeyCode)
                {
                    case Keys.Up:
                        lastChar();
                        break;
                    case Keys.Down:
                        nextChar();
                        break;
                    case Keys.Space:
                        confirm();
                        break;
                    case Keys.Right:
                        nextLabel();
                        break;
                    case Keys.Left:
                        lastLabel();
                        break;
                    case Keys.Escape:
                        cardId = null;
                        goToControl(new Main(), this);
                        break;
                }
                updateLabelCharacters();
            }
        }

        public void confirm()
        {
            if (badWords.Contains(String.Join("", name)))
            {
                DialogResult result = rejectionDialog.Show(new String(name));
            }
            else
            {
                canInput = false;
                DialogResult result = confirmationDialog.Show(new String(name));
                if (result == DialogResult.Cancel)
                {
                    canInput = true;
                }
                else
                {
                    currentPlayerName = String.Join("", name);
                    createUser();
                    goToControl(new GameSelection(), this);
                }
            }
        }

        private void createUser()
        {
            var values = new Dictionary<String, String>
            {
                {"name", currentPlayerName },
                {"cardId", cardId }
            };
            var content = new FormUrlEncodedContent(values);
            try
            {
                Task<HttpResponseMessage> httpResponse = httpClient.PostAsync($"http://{SERVER_IP}:{PORT}/api/students/createStudent", content);
                JObject response = JObject.Parse(httpResponse.Result.Content.ReadAsStringAsync().Result);
            }
            catch (AggregateException)
            {
                var main = new Main();
                main.showInvalidError("No Connection To Game Server");
                goToControl(main, this);
            }
            catch { }
        }

        private void lastChar()
        {
            if (Convert.ToUInt16(name[currentcharacter]) - 1 > 64)
            {
                name[currentcharacter] = (Char)(Convert.ToUInt16(name[currentcharacter]) - 1);
            }
            else
            {
                name[currentcharacter] = (Char)90;
            }
            resetTimerInterval();
        }

        private void nextChar()
        {
            if (Convert.ToUInt16(name[currentcharacter]) + 1 < 91)
            {
                name[currentcharacter] = (Char)(Convert.ToUInt16(name[currentcharacter]) + 1);
            }
            else
            {
                name[currentcharacter] = (Char)65;
            }
            resetTimerInterval();
        }

        private void nextLabel()
        {
            characters[currentcharacter].BackColor = Color.FromArgb(255, 158, 25, 6);
            currentcharacter += 1;
            if (currentcharacter == characters.Count())
            {
                currentcharacter = 0;
            }
            characters[currentcharacter].BackColor = Color.FromArgb(255, 197, 78, 39);
            resetTimerInterval();
        }

        private void lastLabel()
        {
            characters[currentcharacter].BackColor = Color.FromArgb(255, 158, 25, 6);
            currentcharacter -= 1;
            if (currentcharacter == -1)
            {
                currentcharacter = characters.Count() - 1;
            }
            characters[currentcharacter].BackColor = Color.FromArgb(255, 197, 78, 39);
            resetTimerInterval();
        }
        public void updateLabelCharacters()
        {
            characters[currentcharacter].Text = name[currentcharacter].ToString();
        }

        private void highlightCharacterTimer_Tick(object sender, EventArgs e)
        {
            setColorHighlight(characters[currentcharacter]);
        }

        public void resetTimerInterval()
        {
            highlightCharacterTimer.Stop();
            highlightCharacterTimer.Start();
        }

        public void setColorHighlight(Label characterLabel)
        {
            if (characterLabel.BackColor == Color.FromArgb(255, 158, 25, 6))
            {
                characterLabel.BackColor = Color.FromArgb(255, 197, 78, 39);
            }
            else
            {
                characterLabel.BackColor = Color.FromArgb(255, 158, 25, 6);
            }
        }
    }
}
