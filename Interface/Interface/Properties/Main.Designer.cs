﻿namespace Interface.Properties
{
    partial class Main
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.PleaseScanLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.mainMenuTitle = new System.Windows.Forms.PictureBox();
            this.cardInput = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.mainMenuTitle)).BeginInit();
            this.SuspendLayout();
            // 
            // PleaseScanLabel
            // 
            this.PleaseScanLabel.BackColor = System.Drawing.Color.Transparent;
            this.PleaseScanLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PleaseScanLabel.Font = new System.Drawing.Font("Endless Boss Battle", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PleaseScanLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(78)))), ((int)(((byte)(39)))));
            this.PleaseScanLabel.Location = new System.Drawing.Point(0, 844);
            this.PleaseScanLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PleaseScanLabel.Name = "PleaseScanLabel";
            this.PleaseScanLabel.Size = new System.Drawing.Size(2000, 237);
            this.PleaseScanLabel.TabIndex = 5;
            this.PleaseScanLabel.Text = "Scan Student Card Or Press Green Button";
            this.PleaseScanLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // mainMenuTitle
            // 
            this.mainMenuTitle.Image = ((System.Drawing.Image)(resources.GetObject("mainMenuTitle.Image")));
            this.mainMenuTitle.Location = new System.Drawing.Point(456, 315);
            this.mainMenuTitle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mainMenuTitle.Name = "mainMenuTitle";
            this.mainMenuTitle.Size = new System.Drawing.Size(1070, 346);
            this.mainMenuTitle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.mainMenuTitle.TabIndex = 6;
            this.mainMenuTitle.TabStop = false;
            // 
            // cardInput
            // 
            this.cardInput.Location = new System.Drawing.Point(810, 737);
            this.cardInput.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cardInput.Name = "cardInput";
            this.cardInput.Size = new System.Drawing.Size(408, 31);
            this.cardInput.TabIndex = 7;
            this.cardInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(25)))), ((int)(((byte)(6)))));
            this.Controls.Add(this.cardInput);
            this.Controls.Add(this.mainMenuTitle);
            this.Controls.Add(this.PleaseScanLabel);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Main";
            this.Size = new System.Drawing.Size(2000, 1081);
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainMenuTitle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label PleaseScanLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox mainMenuTitle;
        private System.Windows.Forms.TextBox cardInput;
    }
}
