import React, { Component } from "react";

import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import { red500, redA100 } from "material-ui/styles/colors";

import GamePage from "./pages/GamePage";
import NewGamePage from "./pages/NewGamePage";

const url = "http://138.197.136.94:8081";
// const url = "http://localhost:8081";

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: red500,
    accent1Color: redA100
  }
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      destRoute: ""
    };
  }

  render() {
    return (
      <div className="App">
        <Router>
          <MuiThemeProvider muiTheme={muiTheme}>
            <div>
              <Switch>
                <Route
                  exact
                  path="/"
                  render={() => <Redirect push to="/games" />}
                />
                <Route
                  exact
                  path="/games"
                  render={() => <GamePage url={url} />}
                />
                <Route
                  exact
                  path="/newGame"
                  render={() => <NewGamePage url={url} />}
                />
                <Route
                  exact
                  path="/editGame"
                  render={() => <NewGamePage url={url} />}
                />
              </Switch>
            </div>
          </MuiThemeProvider>
        </Router>
      </div>
    );
  }
}

export default App;
