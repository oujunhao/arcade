import * as React from "react";

import GameCard from "./GameCard";

class GameList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      games: []
    };
  }

  componentDidMount() {
    this.getGames();
  }

  getGames = async () => {
    await fetch(`${this.props.url}/api/games/`)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            games: result.foundGames
          });
        },
        error => {
          this.setState({ isLoaded: true, error });
        }
      );
  }

  render() {
    return (
      <div>
        {this.state.games.map((game, i) => (
          <GameCard
            url={this.props.url}
            game={game}
            getGames={this.getGames}
            key={i}
          />
        ))}
      </div>
    );
  }
}

export default GameList;
