import * as React from "react";

import CopyToClipboard from "react-copy-to-clipboard";

import { Link } from "react-router-dom";

import { Card, CardActions, CardHeader, CardText } from "material-ui/Card";
import FlatButton from "material-ui/FlatButton";
import FontIcon from "material-ui/FontIcon";
import Dialog from "material-ui/Dialog";
import TextField from "material-ui/TextField";
import Snackbar from "material-ui/Snackbar";
import { yellow700 } from "material-ui/styles/colors";

class GameCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isExpanded: false,
      gameToken: "",
      showConfirm: false,
      showGameToken: false,
      showCopied: false
    };
  }

  handleGetKey = async () => {
    await fetch(
      `${this.props.url}/api/games/enterGame/${this.props.game._id}/1`
    )
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            gameToken: result.gameToken,
            showGameToken: true
          });
        },
        error => {
          this.setState({ error });
        }
      );
  };

  handleDelete = async () => {
    await fetch(`${this.props.url}/api/games/${this.props.game._id}`, {
      method: "DELETE"
    }).then(res => console.log(res));
    this.props.getGames();
    this.setState({
      showConfirm: false
    });
  };

  render() {
    const actions = [
      <CopyToClipboard
        text={`const string gameToken = "${
          this.state.gameToken
        }";\npublic static Service service = new Service(Environment.GetCommandLineArgs(), gameToken);`}
        onCopy={() => {
          this.setState({ showCopied: true });
        }}
      >
        <FlatButton label="Copy" primary={true} />
      </CopyToClipboard>,
      <FlatButton
        label="Close"
        primary={true}
        onClick={() => this.setState({ showGameToken: false })}
      />
    ];

    let featuredIcon = (
      <FontIcon className="material-icons" color={yellow700}>
        {this.props.game.featured ? "star" : "star_outline"}
      </FontIcon>
    );

    return (
      <div>
        <Dialog
          title={`Game token for ${this.props.game.name}`}
          actions={actions}
          modal={false}
          open={this.state.showGameToken}
          onRequestClose={() => this.setState({ showGameToken: false })}
        >
          <TextField multiLine fullWidth value={this.state.gameToken} />
        </Dialog>
        <Dialog
          title="Delete game?"
          actions={[
            <FlatButton
              primary
              label="Cancel"
              onClick={() => this.setState({ showConfirm: false })}
            />,
            <FlatButton primary label="Delete" onClick={this.handleDelete} />
          ]}
          modal={false}
          open={this.state.showConfirm}
          onRequestClose={() => this.setState({ showConfirm: false })}
        >
          <p>This is permanent</p>
        </Dialog>
        <Snackbar
          open={this.state.showCopied}
          message="Code copied to clipboard"
          autoHideDuration={3000}
          onRequestClose={() => this.setState({ showCopied: false })}
        />
        <div style={{ margin: "auto", maxWidth: "800px" }}>
          <Card style={{ margin: "1rem" }}>
            <CardHeader
              title={this.props.game.name}
              subtitle={this.props.game.publisher}
              avatar={featuredIcon}
            />

            <CardText>{this.props.game.description}</CardText>
            <CardActions>
              <FlatButton
                primary
                label="Get Token"
                onClick={this.handleGetKey}
                containerElement={<div />}
              />
              <FlatButton
                primary
                label="Edit Game"
                containerElement={
                  <Link
                    to={{ pathname: "/editGame", state: this.props.game }}
                  />
                }
              />

              <FlatButton
                label="Delete"
                onClick={() => this.setState({ showConfirm: true })}
                style={{ float: "right" }}
              />
            </CardActions>
          </Card>
        </div>
      </div>
    );
  }
}

export default GameCard;
