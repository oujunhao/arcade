import * as React from "react";

import { Link } from "react-router-dom";

import AppBar from "material-ui/AppBar";
import FloatingActionButton from "material-ui/FloatingActionButton";
import FontIcon from "material-ui/FontIcon";

import GameList from "../components/GameList";

class GamePage extends React.Component {

  render() {
    return (
      <div>
        <AppBar title="Arcade Manager" />
        <GameList url={this.props.url} />
        <FloatingActionButton
          style={{ position: "fixed", right: "1rem", bottom: "1rem" }}
          tooltip="New game"
          containerElement={<Link to="/newGame"/>}
        >
          <FontIcon className="material-icons"> add </FontIcon>
        </FloatingActionButton>
      </div>
    );
  }
}

export default GamePage;
