import * as React from "react";

import { Link, withRouter } from "react-router-dom";

import AppBar from "material-ui/AppBar";
import FlatButton from "material-ui/FlatButton";
import IconButton from "material-ui/IconButton";
import Checkbox from "material-ui/Checkbox";
import FontIcon from "material-ui/FontIcon";
import TextField from "material-ui/TextField";
import DatePicker from "material-ui/DatePicker";
import { Card, CardActions } from "material-ui/Card";
import CircularProgress from "material-ui/CircularProgress";
import { yellow700 } from "material-ui/styles/colors";

class NewGamePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      featured: false,
      name: '',
      publisher: '',
      description: '',
      previewImagePath: 'C:\\games\\',
      exePath: 'C:\\games\\',
      date: new Date(),
      loading: false,
      updating: !!props.location.state
    };
    console.log(props.location.state);
  }

  componentDidMount = () => {
    if (this.props.location.state) {
      this.setState((oldState) => {
        return {
          ...oldState,
          ...this.props.location.state
        }
      });
    }
  }

  setFeatured = () =>
    this.setState(oldState => {
      return {
        featured: !oldState.featured
      };
    });

  handleInputChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleDateChange = (event, date) => {
    this.setState({
      date
    });
  };

  handleNewUntilChange = (event, newUntil) => {
    this.setState({
      newUntil
    });
  };

  handleCreateGame = async e => {
    e.preventDefault();
    this.setState({ loading: true });
    const {
      _id,
      name,
      publisher,
      date,
      newUntil,
      description,
      featured,
      previewImagePath,
      exePath
    } = this.state;
    await fetch(`${this.props.url}/api/games/`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        _id,
        name,
        publisher,
        date,
        newUntil,
        description,
        featured,
        previewImagePath,
        exePath
      })
    }).then(console.log);
    this.props.history.goBack();
  };

  render() {
    const backIcon = (
      <IconButton containerElement={<Link to="/games" />}>
        <FontIcon className="material-icons">arrow_back</FontIcon>
      </IconButton>
    );
    if (this.state.loading) {
      return (
        <div>
          <CircularProgress />
        </div>
      );
    }
    return (
      <div>
        <AppBar
          title={`${this.state.updating ? "Update" : "New"} Game`}
          iconElementLeft={backIcon}
        />
        <div style={{ margin: "1rem auto", maxWidth: "800px" }}>
          <Card
            style={{
              margin: "1rem",
              paddingLeft: "1rem",
              paddingRight: "1rem"
            }}
          >
            <TextField
              fullWidth
              style={{ margin: "auto" }}
              floatingLabelText="Game name"
              name="name"
              value={this.state.name}
              onChange={this.handleInputChange}
            />
            <TextField
              fullWidth
              style={{ margin: "auto" }}
              floatingLabelText="Publisher"
              name="publisher"
              value={this.state.publisher}
              onChange={this.handleInputChange}
            />
            <Checkbox
              label="Featured"
              checkedIcon={
                <FontIcon color={yellow700} className="material-icons">
                  star
                </FontIcon>
              }
              uncheckedIcon={
                <FontIcon color={yellow700} className="material-icons">
                  star_outline
                </FontIcon>
              }
              checked={this.state.featured}
              onCheck={this.setFeatured}
            />
            <DatePicker
              value={this.state.date ? new Date(this.state.date) : Date.now}
              onChange={this.handleDateChange}
              autoOk
              hintText="Date Published"
              fullWidth
            />
            <DatePicker
              value={this.state.newUntil ? new Date(this.state.newUntil) : null}
              onChange={this.handleNewUntilChange}
              autoOk
              hintText="New until (optional)"
              fullWidth
            />
            <TextField
              fullWidth
              multiLine
              style={{ margin: "auto" }}
              floatingLabelText="Description"
              name="description"
              value={this.state.description}
              onChange={this.handleInputChange}
            />
            <TextField
              fullWidth
              style={{ margin: "auto" }}
              floatingLabelText="Preview image path"
              name="previewImagePath"
              value={this.state.previewImagePath}
              onChange={this.handleInputChange}
            />
            <TextField
              fullWidth
              style={{ margin: "auto" }}
              floatingLabelText="Executable path"
              name="exePath"
              value={this.state.exePath}
              onChange={this.handleInputChange}
            />
            <CardActions style={{ overflow: "auto" }}>
              <FlatButton
                style={{ float: "right" }}
                primary
                label={this.state.updating ? "Update" : "Create"}
                onClick={this.handleCreateGame}
              />
              <FlatButton
                style={{ float: "right" }}
                primary
                label="Cancel"
                containerElement={<Link to="/games" />}
              />
            </CardActions>
          </Card>
        </div>
      </div>
    );
  }
}

export default withRouter(NewGamePage);
