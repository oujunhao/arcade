﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace GameSystemServices
{
    public class Service
    {
        string PORT = "8081"; // Default port that the NodeJS API is running on, set for testing
        string SERVER_IP = "138.197.136.94"; // Default IP that the NodeJS API is running on, set for testing
        //These will be automatically overridden when called with command-line arguments
        HttpClient httpClient = new HttpClient(); // Client for sending and recieving HTTP content
        private string gameToken; // JWT to hold cardId and gameId
        private string sessionToken; // JWT to hold sessionId for every game played
        public bool IsGameRunning { get; private set; } = false; // Check if game is running; starts as false
        public bool WasPersonalHighscore { get; private set; } = false; // Was the last score a personal highscore?
        public bool WasGlobalHighscore { get; private set; } = false; // Was the last score a highscore for the entire game?

        // CONSTRUCTOR
        public Service(string[] args, string GameToken = "")
        {
            if (args.Length >= 4) // If a port is supplied by the interface
                PORT = args[3];
            if (args.Length >= 3) // If a server ip address is supplied
                SERVER_IP = args[2];
            if (args.Length >= 2)
                gameToken = args[1]; // If a gameToken is supplied
            else
            {
                Debug.Assert(gameToken != null, "No gameToken supplied. Get it at http://138.197.136.94:5000/games");
                gameToken = GameToken;
            }
        }

        public async void startGame()
        {
            Debug.Assert(!IsGameRunning, "A game is already in progress.");

            IsGameRunning = true;

            var values = new Dictionary<string, string>
            {
                {"gameToken", gameToken },
            };
            var content = new FormUrlEncodedContent(values);

            string stringResponse = "";

            try
            {
                Task<HttpResponseMessage> httpResponse = httpClient.PostAsync($"http://{SERVER_IP}:{PORT}/api/sessions/startSession", content);
                stringResponse = await httpResponse.Result.Content.ReadAsStringAsync();
                JObject response = JObject.Parse(stringResponse);
                sessionToken = response["sessionToken"].ToString();
            }
            catch (Exception)
            {
                throw new HttpRequestException("Could not parse server response, " + stringResponse);
            }
        }

        public async void endGame(int score = 0)
        {
            Debug.Assert(IsGameRunning, "No game has been started.");

            IsGameRunning = false;

            var values = new Dictionary<string, string>
            {
                {"gameToken", gameToken },
                {"sessionToken", sessionToken },
                {"score", score.ToString() },
            };
            var content = new FormUrlEncodedContent(values);

            try
            {
                Task<HttpResponseMessage> httpResponse = httpClient.PostAsync($"http://{SERVER_IP}:{PORT}/api/sessions/endSession", content);
                JObject response = JObject.Parse(await httpResponse.Result.Content.ReadAsStringAsync());

                WasPersonalHighscore = bool.Parse(response["isPersonalHighscore"].ToString());
                WasGlobalHighscore = bool.Parse(response["isGlobalHighscore"].ToString());
            }
            catch (Exception error)
            {
                throw error;
            }
        }

        public IList<Highscore> getHighscores(int limit = 5, int skip = 0)
        {
            Debug.Assert(limit >= 0 && skip >= 0, "Cannot skip or limit to negative numbers.");
            try
            {
                string url = $"http://{SERVER_IP}:{PORT}/api/games/{gameToken}/scores/{skip}/{limit}";
                var responseString = httpClient.GetStringAsync(url);
                JObject response = JObject.Parse(responseString.Result); // Parse result into JSON object
                IList<JToken> serialHighscores = response["highscores"].Children().ToList(); // Get list of highscores from JSON response
                IList<Highscore> highscores = serialHighscores.Select( // Convert JSON to GameSystemServices.Highscore object list
                    serialHighscore => serialHighscore.ToObject<Highscore>())
                    .ToList<Highscore>();
                return highscores;
            }
            catch (Exception)
            {
                var dummyHighscores = new List<Highscore>();
                var dummyScore = new Highscore();
                dummyScore.Rank = 0;
                dummyScore.Name = "No connection to server";
                dummyScore.Score = 0;
                dummyHighscores.Add(dummyScore);
                return dummyHighscores;
            }
        }
    }
}
